1) composer install
2) Edit .env and set your database connection details
3) php artisan key:generate
4) php artisan jwt:secret
5) php artisan migrate
6) npm install
7) php artisan db:seed --class=DatabaseSeeder

# start Laravel
php artisan serve

# start Nuxt
npm run dev/npm run build

# to generate swagger doc
php artisan l5-swagger:generate

# swagger uri
/api/documentation

# commands
php artisan dadata:enrich {id?}
php artisan dadata:coord {id?}
