{
    "openapi": "3.0.0",
    "info": {
        "title": "L5 OpenApi",
        "description": "L5 Swagger OpenApi description",
        "contact": {
            "email": "darius@matulionis.lt"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "1.0.0"
    },
    "paths": {
        "/api/pupils": {
            "get": {
                "description": "Get all pupils",
                "operationId": "App\\Http\\Controllers\\PupilsController::index",
                "responses": {
                    "default": {
                        "description": "Get all pupils",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Returns when user is not authenticated",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not authorized"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            },
            "post": {
                "description": "Add pupil",
                "operationId": "App\\Http\\Controllers\\PupilsController::store",
                "requestBody": {
                    "$ref": "#/components/requestBodies/Pupil"
                },
                "responses": {
                    "default": {
                        "description": "Add pupil",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Returns when user is not authenticated",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not authorized"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    },
                    "422": {
                        "description": "Wrong credentials response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Sorry, wrong credentials. Please try again"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            }
        },
        "/api/pupils/{pupil}": {
            "get": {
                "description": "Get pupil by id",
                "operationId": "App\\Http\\Controllers\\PupilsController::show",
                "parameters": [
                    {
                        "name": "pupil",
                        "in": "path",
                        "description": "pupil id",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "default": {
                        "description": "Get pupil by id",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Returns when user is not authenticated",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not authorized"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            },
            "put": {
                "description": "Update pupil",
                "operationId": "App\\Http\\Controllers\\PupilsController::update",
                "parameters": [
                    {
                        "name": "pupil",
                        "in": "path",
                        "description": "pupil id",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "requestBody": {
                    "$ref": "#/components/requestBodies/Pupil"
                },
                "responses": {
                    "default": {
                        "description": "Update pupil",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Returns when user is not authenticated",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not authorized"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    },
                    "422": {
                        "description": "Wrong credentials response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Sorry, wrong credentials. Please try again"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            },
            "delete": {
                "description": "Delete pupil",
                "operationId": "App\\Http\\Controllers\\PupilsController::destroy",
                "parameters": [
                    {
                        "name": "pupil",
                        "in": "path",
                        "description": "pupil id",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "default": {
                        "description": "Delete pupil",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Returns when user is not authenticated",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not authorized"
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            }
        }
    },
    "components": {
        "schemas": {
            "Pupil": {
                "title": "Pupil add model",
                "description": "Pupil add model",
                "required": [
                    "fio",
                    "phone",
                    "email",
                    "address"
                ],
                "properties": {
                    "fio": {
                        "description": "Username",
                        "type": "string"
                    },
                    "phone": {
                        "description": "Phone",
                        "type": "string"
                    },
                    "email": {
                        "description": "Email",
                        "type": "string",
                        "format": "email",
                        "example": "user1@mail.com"
                    },
                    "address": {
                        "description": "Address",
                        "type": "string"
                    }
                },
                "type": "object"
            }
        },
        "requestBodies": {
            "Pupil": {
                "description": "Pupil object that needs to be added to the database",
                "required": true,
                "content": {
                    "application/json": {
                        "schema": {
                            "$ref": "#/components/schemas/Pupil"
                        }
                    }
                }
            }
        },
        "securitySchemes": {
            "bearerAuth": {
                "type": "http",
                "scheme": "bearer"
            }
        }
    },
    "security": [
        []
    ]
}