<?php

namespace App\Console\Commands;

use App\Models\Pupil;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DaDataPupilNormalizationCommand extends Command
{
    protected $signature = 'dadata:enrich {id?}';

    protected $description = 'enrich';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        var_dump(
            $this->argument('id') ?
                $this->getOne($this->argument('id')) : $this->getAll()
        );
    }

    private function getOne($id)
    {
        $pupil = Pupil::find($id);

        if (!$pupil) {
            throw new ModelNotFoundException();
        }

        return $pupil->enrich();
    }

    private function getAll()
    {
        $pupils = Pupil::all();

        $result = [];

        foreach ($pupils as $pupil) {
            array_push($result, $pupil->enrich());
        }

        return $result;
    }
}
