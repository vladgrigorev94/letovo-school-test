<?php

namespace App\Console\Commands;

use App\Models\Pupil;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AddCoordsToPupilsCommand extends Command
{
    protected $signature = 'dadata:coord {id?}';

    protected $description = 'coord';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        var_dump(
            $this->argument('id') ?
                $this->getOne($this->argument('id')) : $this->getAll()
        );
    }

    private function getOne($id)
    {
        $pupil = Pupil::find($id);

        if (!$pupil) {
            throw new ModelNotFoundException();
        }

        return $pupil->addCoordinatesModelAndGet();
    }

    private function getAll()
    {
        $pupils = Pupil::all();

        $result = [];

        foreach ($pupils as $pupil) {
            array_push($result, $pupil->addCoordinatesModelAndGet());
        }

        return $result;
    }
}
