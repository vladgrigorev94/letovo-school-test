<?php

/**
 * @license Apache 2.0
 */

/**
 *
 * @OA\RequestBody(
 *     request="Pupil",
 *     description="Pupil object that needs to be added to the database",
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/Pupil"),
 * ),
 */

