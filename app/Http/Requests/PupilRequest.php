<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PupilRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fio' => [
                'required'
            ],
            'phone' => [
                'required'
            ],
            'email' => [
                'required',
                'email',
                'unique:pupils'
            ],
            'address' => [
                'required'
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'fio' => [
                'description' => 'fio',
                'example' => 'Kaitlyn Kutch'
            ],
            'phone' => [
                'description' => 'phone',
                'example' => '(609) 809-9325'
            ],
            'email' => [
                'description' => 'email',
                'example' => 'virgie.shields@gmail.com'
            ],
            'address' => [
                'description' => 'address',
                'example' => '7133 Bruce Islands Koeppmouth, IA 81977-0786'
            ],
        ];
    }
}
