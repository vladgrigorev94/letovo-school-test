import axios from 'axios'

export const state = () => ({
  pupils: [],
  perPage: null,
  currentPage: null,
  totalRows: null
})

// getters
export const getters = {
  pupils: state => state.pupils,
  perPage: state => state.perPage,
  currentPage: state => state.currentPage,
  totalRows: state => state.totalRows
}

// mutations
export const mutations = {
  SET_PER_PAGE(state, perPage) {
    state.perPage = perPage
  },
  SET_CURRENT_PAGE(state, currentPage) {
    state.currentPage = currentPage
  },
  SET_TOTAL(state, total) {
    state.totalRows = total
  },
  SET_PUPILS(state, pupils) {
    state.pupils = pupils
  }
}

// actions
export const actions = {
  async fetchPupils({ commit }, { page }) {
    try {
      const { data } = await axios.get('/pupils?page=' + page)
      console.log('data', data)
      commit('SET_PER_PAGE', data.meta.per_page)
      commit('SET_CURRENT_PAGE', data.meta.current_page)
      commit('SET_TOTAL', data.meta.total)
      commit('SET_PUPILS', data.data)
    } catch (e) {
      alert('error')
    }
  },
  async updatePupil({ commit }, { id, payload }) {
    try {
      const { data } = await axios.put('/pupils/' + id, payload)

      if (data !== 1) {
        throw 'not updated'
      }
    } catch (e) {
      alert('error')
    }
  },
}
